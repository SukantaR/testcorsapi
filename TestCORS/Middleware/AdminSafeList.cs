﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TestCORS.Middleware
{
    public class AdminSafeList
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<AdminSafeList> _logger;
        private readonly byte[][] _safelist;

        public AdminSafeList(RequestDelegate next, ILogger<AdminSafeList> logger, string safelist)
        {
            var ips = safelist.Split(';');
            _safelist = new byte[ips.Length][];
            for (var i = 0; i < ips.Length; i++)
            {
                _safelist[i] = IPAddress.Parse(ips[i]).GetAddressBytes();
            }

            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Method != HttpMethod.Get.Method)
            {
                var remoteIp = context.Connection.RemoteIpAddress;

                StringBuilder sb = new StringBuilder();
                sb.Append("\r\nAccess details...");
                sb.Append("\r\nRemoteIP: " + remoteIp + ",");
                sb.Append("\r\nAccept: " + context.Request.Headers["Accept"].ToString() + ",");
                sb.Append("\r\nAccept - Encoding: " + context.Request.Headers["Accept-Encoding"].ToString() + ",");
                sb.Append("\r\nAccept - Language: " + context.Request.Headers["Accept-Language"].ToString() + ",");
                sb.Append("\r\nConnection: " + context.Request.Headers["Connection"].ToString() + ",");
                sb.Append("\r\nContent - Length: " + context.Request.Headers["Content-Length"].ToString() + ",");
                sb.Append("\r\nContent - Type: " + context.Request.Headers["Content-Type"].ToString() + ",");
                sb.Append("\r\nHost: " + context.Request.Headers["Host"].ToString() + ",");
                sb.Append("\r\nReferer: " + context.Request.Headers["Referer"].ToString() + ",");
                sb.Append("\r\nUser - Agent: " + context.Request.Headers["User-Agent"].ToString() + ",");
                sb.Append("\r\nsec - ch - ua: " + context.Request.Headers["sec-ch-ua"].ToString() + ",");
                sb.Append("\r\nDNT: " + context.Request.Headers["DNT"].ToString() + ",");
                sb.Append("\r\nsec - ch - ua - mobile: " + context.Request.Headers["sec-ch-ua-mobile"].ToString() + ",");
                sb.Append("\r\nOrigin: " + context.Request.Headers["Origin"].ToString() + ",");
                sb.Append("\r\nSec - Fetch - Site: " + context.Request.Headers["Sec-Fetch-Site"].ToString() + ",");
                sb.Append("\r\nSec - Fetch - Mode: " + context.Request.Headers["Sec-Fetch-Mode"].ToString() + ",");
                sb.Append("\r\nSec - Fetch - Dest: " + context.Request.Headers["Sec-Fetch-Dest"].ToString());

                _logger.LogInformation(sb.ToString());
                sb.Clear();

                var bytes = remoteIp.GetAddressBytes();
                var badIp = true;
                foreach (var address in _safelist)
                {
                    if (address.SequenceEqual(bytes))
                    {
                        badIp = false;
                        break;
                    }
                }

                if (badIp)
                {
                    _logger.LogWarning("Forbidden Request from Remote IP address: {RemoteIp}", remoteIp);
                    context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    return;
                }
            }

            await _next.Invoke(context);
        }
    }
}
