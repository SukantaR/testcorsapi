﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCORS.Middleware
{
    public class ApiAuthorizeAttribute : TypeFilterAttribute
    {
        public ApiAuthorizeAttribute(string[] origins) : base(typeof(ApiAuthorizeFilter))
        {
            Arguments = new object[] { origins };
        }
    }

    public class ApiAuthorizeFilter : IAuthorizationFilter
    {
        readonly string[] _origins;

        public ApiAuthorizeFilter(string[] origins)
        {
            _origins = origins;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (_origins == null)
                return;

            string referer = context.HttpContext.Request.Headers["Referer"].ToString();
            if (string.IsNullOrWhiteSpace(referer) || !_origins.Any(origin => referer.StartsWith(origin, StringComparison.OrdinalIgnoreCase)))
                context.Result = new ForbidResult();
        }
    }
}
