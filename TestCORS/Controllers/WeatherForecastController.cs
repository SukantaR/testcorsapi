﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCORS.Middleware;

namespace TestCORS.Controllers
{
    [ApiController]
    [EnableCors("_allowOrigin")]
    [ApiAuthorize(new string[] { "http://localhost:9999" })]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        [Route("api/weather")]
        
        public WeatherForecast CreateWeatherReport([FromBody] WeatherForecast weather)
        {
            weather.Status = "Submitted";
            return weather;
        }
    }
}
