using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCORS.Middleware;

namespace TestCORS
{
    public class Startup
    {
        readonly string _allowSpecificOrigins = "_allowOrigin";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors(policy =>
            {
                policy.AddPolicy(name: _allowSpecificOrigins, options => options.WithOrigins("http://localhost:9999").AllowAnyHeader().AllowAnyMethod());
            });

            //services.AddCors(policy =>
            //{
            //    policy.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseMiddleware<AdminSafeList>(Configuration["AdminSafeList"]);
            app.UseCors(options => options.WithOrigins("http://localhost:9999").AllowAnyHeader().AllowAnyMethod());
            app.UseAuthorization();

            loggerFactory.AddFile("Logs/Log-{Date}.txt");
            //app.UseCors(options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireCors(_allowSpecificOrigins);
            });
        }
    }
}
